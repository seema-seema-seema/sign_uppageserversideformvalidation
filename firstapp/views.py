from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth.models import User
from firstapp.models import signup_model
from firstapp.forms import StudentRegistration

# validations by django backend server


def signup(request):
    fm = StudentRegistration()
    if request.method == 'POST':
        fm = StudentRegistration(request.POST)
        if fm.is_valid():
            print("form validated")
            fname = fm.cleaned_data['firstname']
            lname = fm.cleaned_data['lastname']
            uname = fm.cleaned_data['usrname']
            passw = fm.cleaned_data['password']

            em = fm.cleaned_data['email']
            cont = fm.cleaned_data['contact']
            agr = fm.cleaned_data['agre']

            usr = User.objects.create_user(uname, em, passw)
            usr.first_name = fname
            usr.last_name = lname

            sig = signup_model(user=usr, contact_number=cont, agree=agr)
            sig.save()
            return render(request, "userregistration.html", {"status": "Mr./Mrs {} your account created successfully".format(fname)})

    else:
        fm = StudentRegistration()

    return render(request, 'userregistration.html', {'form': fm})
