from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class signup_model(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    contact_number = models.IntegerField(null=True)
    agree = models.BooleanField()

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = "register"
