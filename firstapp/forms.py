from django.core.validators import validate_email
from django.core import validators
from django import forms
from django.core.exceptions import ValidationError
import re
from django.contrib.auth.models import User


def validate_password_strength(value):
    """Validates that a password is as least 10 characters long and has at least
    2 digits and 1 Upper case letter.
    """

    min_length = 10

    if len(value) < min_length:
        raise ValidationError(('Password must be at least {0} characters '
                               'long.').format(min_length))

    # check for 2 digits
    if sum(c.isdigit() for c in value) < 2:
        raise ValidationError(('Password must container at least 2 digits.'))

    # check for uppercase letter
    if not any(c.isupper() for c in value):
        raise ValidationError(
            ('Password must container at least 1 uppercase letter.'))

    return value


class StudentRegistration(forms.Form):  # or forms.ModelForm

    firstname = forms.CharField(
        error_messages={'required': 'First Name is mandatory*'})
    lastname = forms.CharField(empty_value=' ')
    usrname = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput, validators=[
        validate_password_strength])
    Confirm_Password = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField()
    contact = forms.CharField()
    agre = forms.BooleanField(label_suffix='', label='I Agree')

    def clean_Confirm_Password(self):
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('Confirm_Password')
        print("second none paswrd {}".format(password2))

        if password1 != password2:
            raise ValidationError("Password don't match")

    def clean_email(self):
        # if email already exist in database
        data = self.cleaned_data['email']
        r = User.objects.filter(email=data)
        if r.count():
            raise ValidationError("Email already exists")

        # validation
        domain = data.split('@')[1]
        domain_list = ["gmail.com", "yahoo.com", "hotmail.com", ]
        if domain not in domain_list:
            raise ValidationError(
                "Please Enter an Email Address with a valid domain")
        return data

    def clean_contact(self):
        valcont = self.cleaned_data.get("contact")
        Pattern = re.compile("(0/91)?[7-9][0-9]{9}")

        if Pattern.match(valcont) and len(valcont) <= 10:
            print("Valid Number")
        else:
            print("Invalid Number")
            raise ValidationError("Invalid Mobile Number")

    def clean_usrname(self):
        usrname = self.cleaned_data['usrname'].lower()
        r = User.objects.filter(username=usrname)
        if r.count():
            raise ValidationError("Username already exists")
        return usrname
